package com.example.image.authentication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	protected DBHelper dbhelp;	
	protected Button signup;
	protected Button login;
	protected EditText uname;
	protected EditText pword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbhelp = new DBHelper(this);        
        signup = (Button)findViewById(R.id.button2);
        login = (Button)findViewById(R.id.button1);        
        uname = (EditText)findViewById(R.id.editText2);
        pword = (EditText)findViewById(R.id.editText1);
                
        if(checkData() == true){
        	signup.setVisibility(View.INVISIBLE); 
    		Log.d("accounts check", "Values exists in table"); 

        }else{
        	login.setVisibility(View.INVISIBLE);
    		Log.d("accounts check", "No values in table"); 

        }
        
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String UName = uname.getText().toString();
            	String PWord =  md5(pword.getText().toString());
            	addData(UName,PWord);
            	Log.d("Sign Up Button", "UN:" + UName + ". PW:" + PWord);
            	Toast.makeText(getApplicationContext(), UName + ", you have successfully signed up!", Toast.LENGTH_LONG).show();
            	uname.setText("");
            	pword.setText("");
            	signup.setVisibility(View.INVISIBLE);
            	login.setVisibility(View.VISIBLE);
            }
        });
        
        final Intent intent = new Intent(this, MainActivity2.class);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String UName = uname.getText().toString();
            	String PWord =  md5(pword.getText().toString());
            	Integer data = checkData(UName,PWord); 
            	if (data != -1) {
            		intent.putExtra("UID", data);            		
            		startActivity(intent);
            		Log.d("Log In Button", "Login successful");
            		

            	} else {
            		// Enter username/password again
            		Toast.makeText(getApplicationContext(), "Wrong username or password, retry.", Toast.LENGTH_SHORT).show();            		
            		Log.d("Log In Button", "Login failed");
            	}
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    private void addData(String username, String password) {
        SQLiteDatabase db = dbhelp.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHelper.USER, username);
        values.put(DBHelper.PASS, password);
        db.insert(DBHelper.TABLE, null, values);
      }
    
    public Integer checkData(String username, String password) {
    	SQLiteDatabase db = dbhelp.getReadableDatabase();
    	Cursor c = db.rawQuery("SELECT _ID FROM accounts WHERE username= ? AND password= ?", new String[] {username, password});
    	if(c.moveToFirst()) {
    		// at least one row was returned, this should signal success
    		Integer ID = c.getInt(0);    		
    		c.close();
    		return ID;
    	} else {
    		// authentication failed    		
    		c.close();
    		return -1;
    	}
    }
    
    public boolean checkData() {
    	SQLiteDatabase db = dbhelp.getReadableDatabase();
    	Cursor c = db.rawQuery("SELECT * FROM accounts", null);
    	if(c.moveToFirst()) {
    		// at least one row was returned, this should signal success
    		c.close();
    		return true;
    	} else {
    		// 0 rows, this should signal failure
    		c.close();
    		return false;
    	}
    }
    
    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    

}
