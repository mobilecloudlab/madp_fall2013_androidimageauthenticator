package com.example.image.authentication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/** Helper to the database, manages versions and creation */
public class DBHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "accounts.db";
	private static final int DATABASE_VERSION = 1;

	// Table name
	public static final String TABLE = "accounts";

	// Columns
	public static final String USER = "username";
	public static final String PASS = "password";

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "create table " + TABLE + "( " + BaseColumns._ID
				+ " integer primary key autoincrement, " + USER + " text, "
				+ PASS + " text);";
		Log.d("DBHelper", "onCreate: " + sql);
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}