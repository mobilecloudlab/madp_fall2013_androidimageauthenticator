package com.example.image.authentication;

import java.io.File;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineHelper;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;
import ar.com.hjg.pngj.chunks.ChunkHelper;
import ar.com.hjg.pngj.chunks.PngChunk;
import ar.com.hjg.pngj.chunks.PngChunkTextVar;

public class MainActivity2 extends Activity {
	protected Button add;
	protected Spinner selection;
	protected EditText user;
	protected EditText pass;
	protected ImageView authentication_image;
	protected ImageView fb_logo;
	protected ImageView gmail_logo;
	protected Bitmap bmap;
	protected String gmail_user_cache;
	protected String gmail_pass_cache;
	protected String facebook_user_cache;
	protected String facebook_pass_cache;
	protected Uri uri;	
	protected String spinner_value;
	protected WebView webview;

	
	@Override
	 protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		webview = new WebView(this);
		setContentView(R.layout.activity2);
		Button add = (Button)findViewById(R.id.button1);
		selection = (Spinner)findViewById(R.id.spinner1);
		user = (EditText)findViewById(R.id.editText1);
		pass = (EditText)findViewById(R.id.editText2);
		authentication_image = (ImageView)findViewById(R.id.imageView3);		
		authentication_image.setOnTouchListener(new MyTouchListener());
		fb_logo = (ImageView)findViewById(R.id.imageView1);
		fb_logo.setOnDragListener(new MyDragListener());
		gmail_logo = (ImageView)findViewById(R.id.imageView2);
		gmail_logo.setOnDragListener(new MyDragListener());
		uri = Uri.parse(getFilesDir().getPath() + "/img_auth_pic.png");
		if (fileExistance("img_auth_pic.png")) {
			updateImage();
		}
		add.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.i("Nupule vajutus", "-------------------------------------------------------------------");
				spinner_value = selection.getSelectedItem().toString();
				String user_string = user.getText().toString();
				String pass_string = pass.getText().toString();				
					Boolean asd = fileExistance("img_auth_pic.png");
					Log.i("Fail olemas?", asd.toString());					
				if (fileExistance("img_auth_pic.png")) {
					// get data
					String[] x = reader();
					gmail_user_cache = x[0];
					gmail_pass_cache = x[1];
					facebook_user_cache = x[2];
					facebook_pass_cache = x[3];										
					// add data
					createNewImage(user_string, pass_string, gmail_user_cache, gmail_pass_cache, facebook_user_cache, facebook_pass_cache, spinner_value);
					updateImage();													
				} else {	
					createNewImage(user_string, pass_string, "", "", "", "", spinner_value);					
					updateImage();
				}
				user.setText("");
				pass.setText("");
		    	Toast.makeText(getApplicationContext(), spinner_value + " information updated." , Toast.LENGTH_SHORT).show();
			}
		});
	 }

	 @Override
	 public boolean onCreateOptionsMenu(Menu menu) {
		 // Inflate the menu; this adds items to the action bar if it is present.
		 getMenuInflater().inflate(R.menu.main_activity2, menu);
		 return true;
	 }
	 
	 private final class MyTouchListener implements OnTouchListener {
		  public boolean onTouch(View view, MotionEvent motionEvent) {
		    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
		      ClipData data = ClipData.newPlainText("", "");
		      DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
		      view.startDrag(data, shadowBuilder, view, 0);		      
		      return true;
		    } else {
		    return false;
		    }
		  }
		}
	 
	 class MyDragListener implements OnDragListener {		  
		  public boolean onDrag(View v, DragEvent event) {
		    switch (event.getAction()) {
		    case DragEvent.ACTION_DRAG_STARTED:
		    	break;
		    case DragEvent.ACTION_DRAG_ENTERED:
		    	break;
		    case DragEvent.ACTION_DRAG_EXITED:
		    	break;
		    case DragEvent.ACTION_DROP:
		    	String[] x = reader();
		    	if (v != gmail_logo) {		    		
			    	crateWebView("Facebook", x[2], x[3]);
		    	} else {
		    		crateWebView("Gmail", x[0], x[1]);
		    	}
		    	break;
		    case DragEvent.ACTION_DRAG_ENDED:
		    	Toast.makeText(getApplicationContext(), "Please drag the image to Facebook or Gmail logo", Toast.LENGTH_SHORT).show();
		      	default:
		      	break;
		    }
		    return true;
		  }
		} 

	 private void updateImage() {
		 bmap = BitmapFactory.decodeFile(uri.toString());
		 authentication_image.setImageBitmap(bmap);
	 }
	 
	 private boolean fileExistance(String fname){
	    File file = getBaseContext().getFileStreamPath(fname);
	    return file.exists();
	 }

     private File localFile() {
         return new File(Uri.parse(getFilesDir().getPath()).toString(), "img_auth_pic.png");
     }
  
     private void createNewImage(String u, String p, String gu_cache, String gp_cache, String fu_cache, String fp_cache, String spin) {
    	 Integer cols = 5;
    	 Integer rows = 5;
    	 ImageInfo imi = new ImageInfo(cols, rows, 8, false); // 8 bits per channel, no alpha
         // open image for writing to a output stream
         PngWriter png = new PngWriter(localFile(), imi);
         // add some optional metadata (chunks)
         png.getMetadata().setDpi(100.0);
         png.getMetadata().setTimeNow(0); // 0 seconds fron now = now
         if (spin.equals("Gmail")) {
        	 png.getMetadata().setText("gu", u);
             png.getMetadata().setText("gp", p);             
             png.getMetadata().setText("fu", fu_cache);
             png.getMetadata().setText("fp", fp_cache);
         } else {
        	 png.getMetadata().setText("gu", gu_cache);
             png.getMetadata().setText("gp", gp_cache);
        	 png.getMetadata().setText("fu", u);
             png.getMetadata().setText("fp", p);             
         }
         ImageLineInt iline = new ImageLineInt(imi);
         for (int col = 0; col < imi.cols; col++) { // this line will be written to all rows
                 int r = 255;
                 int g = 127;
                 int b = 255 * col / imi.cols;
                 ImageLineHelper.setPixelRGB8(iline, col, r, g, b); // orange-ish gradient
         }
         for (int row = 0; row < png.imgInfo.rows; row++) {
                 png.writeRow(iline);
         }
         png.end();
     }
     
     private String[] reader(){
    	 String[] a = {"","","",""};
    	 PngReader pngr = new PngReader(localFile());
         pngr.readSkippingAllRows();
         Integer ok = 0;
         for (PngChunk c : pngr.getChunksList().getChunks()) {
                 if (!ChunkHelper.isText(c))
                         continue;
                 ok++;
                 PngChunkTextVar ct = (PngChunkTextVar) c;
                 String key = ct.getKey();
                 String val = ct.getVal();
                 if (key.equals("gu")) {
                     a[0] = val;                	 
                 } else if (key.equals("gp")) {
                	 a[1] = val;
                 } else if (key.equals("fu")) {
                	 a[2] = val;
                 } else if (key.equals("fp")) {
                	 a[3] = val;
                 }
         }
         return a;
     }
     
    @SuppressLint("SetJavaScriptEnabled")
	private void crateWebView(final String page, final String user, final String pass){
    	setContentView(webview);
    	String url = "";
    	if (page.equals("Facebook")) {
    		url = "http://www.facebook.com";
    	}
    	else {
    		url = "http://www.gmail.com";
    	}    	
    	WebSettings webSettings = webview.getSettings();
    	webSettings.setPluginState(PluginState.ON);
    	webSettings.setJavaScriptEnabled(true);
    	webview.setWebChromeClient(new WebChromeClient());
    	webview.setWebViewClient(new WebViewClient() {
    		public void onPageFinished(WebView view, String url) {
    			super.onPageFinished(webview, url);
    			if (page.equals("Facebook")) {
    				view.loadUrl("javascript:document.getElementsByName('email')[0].value='" + user + "';");
        			view.loadUrl("javascript:document.getElementsByName('pass')[0].value='" + pass + "';");    				
    			} else {
    				view.loadUrl("javascript:document.getElementById('Email').value='" + user + "';");
        			view.loadUrl("javascript:document.getElementById('Passwd').value='" + pass + "';");
    			}
    		}
    	});
    	webview.loadUrl(url);
     }
 
}