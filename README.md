# Image Authentication
Mihkel Visnapuu - mihkelvis

Rein Torm - rtorm

## Building the project(debug mode) ##

1. Clone this project
2. From command line navigate to the base directory of the project
3. Execute commands:
```
$ ant clean
```
```
$ ant debug
```

## Testing on virtual emulator ##

1. Make sure virtual device is running.(I prefer using Genymotion)
2. From command line navigate to the base directory of the project.
3. Execute commands:
```
$ ant clean
```
```
$ ant debug install
```
4. Run Image Authentication app from emulator.
5. Test application.

## How to use ##

First in order to use the application, the user has to create an account with username and password. 
These are stored in a local database(password will be hashed with md5).
Currently only one account can be created.

Once the user has been authenticated, we can start adding data to the image:
1. Fill in username and password fields.
2. Choose which account(Facebook or Gmail) is going to use these from the spinner.
3. Press 'Add' button.

This will create the image to the Internal storage making sure that this image will be only accessible to the
application. The user will be notified which account was updated.

The image can hold ONE Facebook account information and ONE Gmail account information at any given moment.
If the user wants to **update** the information for either Facebook or Gmail account, all that is needed
is to fill in the username/password fields, choosing the corresponding spinner value and pressing 'Add' button.
Doing this will rewrite the old information in the image for either Facebook or Gmail account in the image(depending
what was the spinner value selection).

In order to log in to Facebook or Gmail all that is necessary is to drag the gradient image to either Facebook or
Gmail logo. This will create a webview and fill in the username/password fields using javascript.

## Known bugs ##

If the image is not created and the 'placeholder image' is dragged to Facebook/Gmail logo, the application will crash -
because there is no data for username/password in the image.